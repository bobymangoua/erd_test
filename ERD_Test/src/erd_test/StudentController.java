/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package erd_test;

/**
 *
 * @author lmangoua
 * Date: 19/09/2016
 */
public class StudentController {
    
    //Composition has-a relationship(StudentController has a Student)
    private Student model;
    private StudentView view;
    
    //Default Constructor
    public StudentController() {
    }
        
    //Constructor
    public StudentController(Student model, StudentView view) {    
        this.model = model;
        this.view = view;
    }
    
    //Getters & Setters
    public String getStudentRollNo() {
        return model.getRollNo();
    }

    public void setStudentRollNo(String rollNo) {
        model.setRollNo(rollNo);
    }

    public String getStudentName() {
        return model.getName();
    }

    public void setStudentName(String name) {
        model.setName(name);
    }
    
    

//    public Student getModel() {
//        return model;
//    }
//
//    public void setModel(Student model) {
//        this.model = model;
//    }
//
//    public StudentView getView() {
//        return view;
//    }
//
//    public void setView(StudentView view) {
//        this.view = view;
//    }
    
    
    
}
